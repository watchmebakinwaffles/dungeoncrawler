# Le sujet de votre jeu

Un jeu de plateforme où le joueur évolue à la lueur de son personnage et de torches dans des niveaux en obscurité totale.
Le joueur doit allumer des torches pour gagner des points et atteindre la sortie pour arriver au niveau suivant. Chaque niveau terminé augmente le multiplicateur de points du niveau suivant mais aussi sa difficulté.
Le joueur possède un halo de lumière qui s'estompe avec le temps. Se tenir proche d'une torche allumée restaure le halo.

Les attaques du joueur (touche A) consomment de la stamina (barre verte). Le joueur peut attaquer les ennemis qui lui barre la route.
Le joueur peut effectuer un saut simple et un double saut (barre d'espace).
Le joueur peut interagir avec le décor lorsqu'un élément interactif est à portée (touche F).
Le joueur se déplace avec les touches de direction.
Le joueur peut s'accroupir.

Le joueur perd lorsqu'il n'a plus de points de vie (barre rouge).

Pour tester le jeu en l'état actuel, ouvrir la scène "MainMenu".

Etat du jeu :
- Un menu "MainMenu"
- Un écran des crédits "Credits"
- Un niveau de test "Sandbox"

# Les problèmes que vous avez pu rencontrer

1. Le prefab instancié par l'objet PlayerStart (désactivé dans la scène Sandbox) réagit bizarrement :
Double saut impossible et propulsions des attaques anarchiques (on dirait qu'un multiplicateur agit sur la force exercée)
(Pour tester : désactiver le Player et réactiver l'objet "PlayerStart")

2. Manque de temps. (projet trop conséquent et mal évalué de ma part pour la période de travail que nous avions)

# les solutions que vous avez pu trouver

1. Seul réel problème non résolu qui me donne encore des noeuds au cerveau.

# Les appareils sur lesquels vous avez testé l'ensemble

Testé sous Windows et WebGL. Lancé sous Android mais sans interaction possible (contrôles pas adaptés).

# Et toutes les informations que vous jugerez pertinentes.

J'ai préféré axer mon travail sur la découverte de fonctionnalités telles que la TileMap et la création d'outils internes destinés à faciliter le Level Design et le Game Design.
(Exemple : la TileMap et les custom Brush ainsi que les Champs sérialisés permettant de gérer facilement toutes les variables des personnages)

Je regrette néanmoins de n'avoir pu créer au moins un ennemi avec son IA ainsi que de n'avoir pu mettre en place la gestion du score et des meilleurs scores avec les singletons et le stockage persistant.



