﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

    [SerializeField] CharacterController2D controller;
    [SerializeField] Animator animator;
    [SerializeField] float moveSpeed = 0f;
    [SerializeField] private Attack attackHandler;

    float horizontalMove = 0f;
    bool jump = false;
    bool crouch = false;
    private bool doubleJump = true;

	// Update is called once per frame
	void Update () {

        animator.SetFloat("Speed", Mathf.Abs(horizontalMove));

        if (Input.GetButtonDown("Jump") && (controller.getGrounded() || doubleJump))
        {
            jump = true;
        }

        if (Input.GetButtonDown("Crouch") && jump==false)
        {
            crouch = true;
        }
        else if (Input.GetButtonUp("Crouch"))
        {
            crouch = false;
        }

        if (!attackHandler.getIsAttacking())
        {
            horizontalMove = Input.GetAxisRaw("Horizontal") * moveSpeed;
        }
        else
        {
            horizontalMove = 0;
            jump = false;
        }

    }

    public void OnLanding()
    {
        animator.SetBool("isFalling", false);
        doubleJump = true;
    }

    public void OnCrouching(bool isCrouching)
    {
        animator.SetBool("isCrouching", isCrouching);
    }

    void FixedUpdate()
    {
        if (jump)
        {
            animator.SetBool("isFalling", false);
            if (controller.getGrounded())
            {
                animator.SetTrigger("jump");
            }
            else
            {
                animator.SetTrigger("roll");
            }

        }else if (controller.getFalling() && !this.animator.GetCurrentAnimatorStateInfo(0).IsName("player_roll"))
        {
            animator.SetBool("isFalling", true);
        }

        controller.Move(horizontalMove * Time.fixedDeltaTime, crouch, jump, doubleJump);

        if (controller.getGrounded())
        {
            animator.SetBool("isGrounded", true);
        }
        else
        {
            animator.SetBool("isGrounded", false);
        }

        if (jump)
        {
            doubleJump = false;
        }
        jump = false;
    }
}
