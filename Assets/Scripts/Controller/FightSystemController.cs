﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FightSystemController : MonoBehaviour {

    [SerializeField] float MaxHealth;
    [SerializeField] float Health;

    [SerializeField] float MaxStamina;
    [SerializeField] float Stamina;

    private float previousHealth;
    private float previousStamina;

    [SerializeField] RectTransform HealthBar;
    [SerializeField] RectTransform StaminaBar;

    private float HBmaxSize;
    private float SBmaxSize;

    // Use this for initialization
    void Start () {
        previousHealth = Health;
        HBmaxSize = HealthBar.rect.width;
        SBmaxSize = StaminaBar.rect.width;
    }
	
	// Update is called once per frame
	void Update () {
        if (Health != 0)
        {
            if (Health != previousHealth)
            {

                if (MaxHealth != Health)
                {
                    HealthBar.offsetMax = new Vector2(HBmaxSize * ((MaxHealth - Health) / MaxHealth) * -1, HealthBar.offsetMax.y);
                }
                else
                {
                    HealthBar.offsetMax = new Vector2(0, HealthBar.offsetMax.y);
                }

            }

            if (Stamina != previousStamina)
            {

                if (MaxStamina != Stamina)
                {
                    StaminaBar.offsetMax = new Vector2(SBmaxSize * ((MaxStamina - Stamina) / MaxStamina) * -1, StaminaBar.offsetMax.y);
                }
                else
                {
                    StaminaBar.offsetMax = new Vector2(0, StaminaBar.offsetMax.y);
                }

            }

            previousHealth = Health;
            previousStamina = Stamina;
        }
        else
        {
            Kill();
        }
  
    }

    void Kill()
    {

    }

    public void setHealthBar(RectTransform hb)
    {
        HealthBar = hb;
    }

    public void setStaminaBar(RectTransform sb)
    {
        StaminaBar = sb;
    }
}
