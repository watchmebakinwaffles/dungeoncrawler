﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Tilemaps;
using UnityEngine;

public class LayerOrder : MonoBehaviour {

    private GameObject parent;

	// Use this for initialization
	void Start () {
        parent = transform.parent.gameObject;
        GetComponent<SpriteRenderer>().sortingOrder = parent.GetComponent<TilemapRenderer>().sortingOrder;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
