﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightMaskDecreaseOverTime : MonoBehaviour {

    [Range(0.0f,4.0f)]
    [SerializeField] float MaxScale = 2.0f;

    [Range(0.0f, 4.0f)]
    [SerializeField] float MinScale = 2.0f;

    [Range(0.01f, 3.0f)]
    [SerializeField] float DecreaseTime;

    [Range(0.01f, 0.05f)]
    [SerializeField] float DecreaseIncrement;

    private bool isInLight = false;

    private Collider2D lastLight;

    float timer = 0;

    // Use this for initialization
    void Start () {
        if (MinScale >= MaxScale)
        {
            MinScale = MaxScale/2;
        }
        transform.localScale = new Vector3(MaxScale,MaxScale,1);
    }
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;
        float IncrementTime;
        if (isInLight)
        {
            IncrementTime = DecreaseTime/10; 
        }
        else
        {
            IncrementTime = DecreaseTime;
        }
        if (timer > IncrementTime)
        {
            if(isInLight && transform.localScale.x <= MaxScale)
            {
                transform.localScale = new Vector3(transform.localScale.x + DecreaseIncrement,
                transform.localScale.y + DecreaseIncrement, transform.localScale.z);
            }
            else if (!isInLight && transform.localScale.x >= MinScale)
            {
                transform.localScale = new Vector3(transform.localScale.x - DecreaseIncrement,
                transform.localScale.y - DecreaseIncrement, transform.localScale.z);
            }
            

            timer = 0;
        }
        if(lastLight == null)
        {
            isInLight = false;
        }
        
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        
        if (col.tag == "SafeLightZone" && col.GetComponent<LightMask>().getState())
        {
            lastLight = col;
            isInLight = true;
        }

    }

    void OnTriggerExit2D(Collider2D col)
    {

        if (col.tag == "SafeLightZone")
        {
            isInLight = false;
        }

    }

}
