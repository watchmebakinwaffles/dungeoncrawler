﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class PlayerStart : MonoBehaviour {

    [SerializeField] private GameObject PlayerCharacter;

    private float sizeY;
    private GameObject player;

    // Use this for pre-initialization
    void Start () {

        sizeY = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;

        player = Instantiate(PlayerCharacter);
        player.transform.position = new Vector3(transform.position.x, transform.position.y + sizeY / 2, transform.position.z);
        player.transform.SetParent(GameObject.FindGameObjectWithTag("Characters").transform);

        GameObject.FindGameObjectWithTag("PlayerCam").GetComponent<CinemachineVirtualCamera>().Follow = player.transform;

        
    }
        
}
