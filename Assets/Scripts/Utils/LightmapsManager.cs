﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class LightmapsManager : MonoBehaviour {

    [SerializeField] Tilemap BlurredMap;
    [SerializeField] Tilemap DarkMap;
    [SerializeField] Tilemap BackgroundMap;

    [SerializeField] Tile BlurredTile;
    [SerializeField] Tile DarkTile;

    // Use this for initialization
    void Start () {
        DarkMap.origin = BlurredMap.origin = BackgroundMap.origin;
        DarkMap.size = BlurredMap.size = BackgroundMap.size;

        foreach(Vector3Int cell in DarkMap.cellBounds.allPositionsWithin)
        {
            DarkMap.SetTile(cell, DarkTile);
        }

        foreach (Vector3Int cell in BlurredMap.cellBounds.allPositionsWithin)
        {
            BlurredMap.SetTile(cell, BlurredTile);
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
