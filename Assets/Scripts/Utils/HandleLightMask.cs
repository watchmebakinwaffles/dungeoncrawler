﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandleLightMask : MonoBehaviour
{

    [SerializeField] private bool isOn = false;

    [SerializeField] private LightMask lightmaskPrefab;

    private LightMask lightmask;

    [SerializeField] private Animator animator;

    // Use this for initialization
    void Start()
    {
        if (isOn)
        {
            isOn = false;
            Interaction();
        }
        
    }

    public void Interaction()
    {
        if (!isOn)
        {
            if (lightmask == null)
            {
                animator.SetTrigger("on");
                isOn = true;
                lightmask = Instantiate(lightmaskPrefab);
                lightmask.setTarget(transform);
                lightmask.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);
                lightmask.transform.SetParent(GameObject.FindGameObjectWithTag("LightMasks").transform);
            }
        }
        else
        {
            if (lightmask != null)
            {
                isOn = false;
                DestroyImmediate(lightmask.gameObject);
                animator.SetTrigger("off");
            }
        }
    }

    public bool getIsOn()
    {
        return isOn;
    }
}
