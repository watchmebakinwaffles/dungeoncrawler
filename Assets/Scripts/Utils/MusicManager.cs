﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour {

    [SerializeField] AudioClip Clip;

    [SerializeField] AudioSource Source;

    // Use this for initialization
    void Start () {
        Source.clip = Clip;
        Source.Play();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
