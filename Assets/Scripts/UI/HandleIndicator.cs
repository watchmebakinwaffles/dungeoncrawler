﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandleIndicator : MonoBehaviour {

    [SerializeField] private GameObject indicator;

    private bool isActive = false;

    void Start()
    {
        // Init GameObject as inactive
        indicator.SetActive(false);
    }

    void FixedUpdate()
    {
        //Debug.Log(lightmask);

    }

    public void SetVisibility()
    {
        //Show or hide in function of current state
        if (!isActive)
        {
            isActive = true;
            indicator.SetActive(true);
        }
        else
        {
            isActive = false;
            indicator.SetActive(false);
        }
    }
}
