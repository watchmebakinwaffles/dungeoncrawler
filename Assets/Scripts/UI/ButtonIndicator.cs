﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonIndicator : MonoBehaviour {

    // List of buttons
    public enum ButtonList
    {
        f,
    }

    // List of sizes
    public enum SizeList
    {
        small,
        medium,
        large
    }

    // Elements to be processed
    [SerializeField] private ButtonList button;
    [SerializeField] private SizeList size;
    [SerializeField] private Animator animator;
    [SerializeField] private GameObject bindObject; // Ref object for positionning

    // States variables
    private string selectedButton;
    private string selectedSize;
    private string clipName;


    void Awake ()
    {
        GetComponent<SpriteRenderer>().enabled = true;
    }

    // Use this for initialization
    void Start () {

        // Set bindObject if null
        if (bindObject == null)
        {
            bindObject = transform.parent.gameObject;
        }

        // Set clipTrigger string
        switch (button)
        {
            case ButtonList.f:
                selectedButton = "f";
                break;
        }

        switch (size)
        {
            case SizeList.small:
                selectedSize = "small";
                break;
            case SizeList.medium:
                selectedSize = "medium";
                break;
            case SizeList.large:
                selectedSize = "large";
                break;
        }

        // Trigger animation
        animator.SetTrigger(selectedSize + "_" + selectedButton);

    }
	
	// Update is called once per frame
	void Update () {

        // Set position for moving objects
        gameObject.transform.position = bindObject.transform.position + new Vector3(bindObject.GetComponent<SpriteRenderer>().size.x/2, bindObject.GetComponent<SpriteRenderer>().size.y/10, 0);

        // Trigger animation on GameObject reactivation
        if (!animator.GetCurrentAnimatorStateInfo(0).IsName("ButtonIndicator_" + selectedSize + "_" + selectedButton))
        {
            animator.SetTrigger(selectedSize + "_" + selectedButton);
        }
    }
}
