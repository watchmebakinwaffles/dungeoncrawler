﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInfo : MonoBehaviour {

    [SerializeField] Transform target;

    [SerializeField] RectTransform HealthBar;
    [SerializeField] RectTransform StaminaBar;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (target != null)
        {
            // Update PlayerInfo position to match target's above head spot

            //Debug.Log(target.gameObject.GetComponent<SpriteRenderer>().bounds.size.y / 1.5);
            //transform.position = Vector3.MoveTowards(transform.position, target.position, 20 * Time.deltaTime);
            transform.position = new Vector3(target.position.x, target.position.y + (float)(target.gameObject.GetComponent<SpriteRenderer>().bounds.size.y / 1.5), target.position.z);
        }
    }

    public void setTarget(Transform targetToSet)
    {
        // Set target
        target = targetToSet;
    }

    public RectTransform getHealthBar()
    {
        return HealthBar;
    }

    public RectTransform getStaminaBar()
    {
        return StaminaBar;
    }
}
